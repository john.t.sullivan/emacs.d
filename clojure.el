
;;; Code:
(use-package clojure-mode
  :ensure t)

(use-package cider
  :ensure t)


(provide 'clojure)
;;; clojure.el ends here
