
;;; Code:

(defconst org-notes (concat (getenv "HOME") "/notes/"))
(defconst zot-bib (concat org-notes ".bib/library.bib"))

(setq
 org-agenda-files (list org-notes)
 org-directory org-notes
 org-roam-directory org-notes
 org-ellipsis " ▼ "
 deft-directory org-notes)

(use-package org
	     :after general
	     :config
	     (general-create-definer org-def :prefix ",")
	     (org-def :states '(normal visual)
	       "a" 'org-agenda)
(font-lock-add-keywords 'org-mode
                        '(("^ *\\([-]\\) "
                           (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•")))))))
(use-package org-bullets
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
 

(use-package org-roam
      :hook
      (after-init . org-roam-mode)
      (org-roam-directory org-notes)
      :config
      (org-def :states '(normal visual)
	     "," 'org-roam
      	     "f" 'org-roam-find-file
	     "F" 'org-roam-capture
      	     "b" 'org-roam-switch-to-buffer
      	     "g" 'org-roam-graph
      	     "i" 'org-roam-insert)
      (setq org-roam-directory org-notes
	   
       org-roam-capture-templates
	    '(("d" "default" plain (function org-roam--capture-get-point)
	       "%?"
	       :file-name "%(format-time-string \"%Y-%m-%d--%H-%M-%SZ--${slug}\" (current-time) t)"
	       :head "#+TITLE: ${title}\n"
	       :unnarrowed t)

	      ("p" "Person" plain (function org-roam--capture-get-point)
	       "%?"
	       :file-name "%(format-time-string \"%Y-%m-%d--%H-%M-%SZ--${slug}\" (current-time) t)"
	       :head "#+TITLE: ${title}\n#+ROAM_TAGS: Person\n"
	       :unnarrowed t))
      ))

(use-package company-org-roam
  :after (company org-roam)
  :ensure t
  :config
  (push 'company-org-roam company-backends))

(use-package org-journal
  :ensure t
  :config
  (org-def :states 'normal
    "j" 'org-journal-new-entry)
  (setq org-journal-date-prefix "#+TITLE: "
  org-journal-file-format "%Y-%m-%d.org"
  org-journal-dir org-notes
  org-journal-date-format "%A, %d %B %Y"))

(use-package deft
  :ensure t
  :after (org org-roam)
  :config
  (org-def :states 'normal
    "d" 'deft
    "s" 'deft-filter
    "S" 'deft-filter-clear
    "RET" 'deft-open-file-other-window )
  (setq deft-recursive t
  deft-use-filter-string-for-filename t
  deft-default-extension "org"
  deft-directory org-notes))

(use-package org-ref
  :after ivy-bibtex
  :ensure t
  :config
  (setq org-ref-completion-library 'org-ref-ivy-cite-completion
	org-ref-get-pdf-filename-function 'org-ref-get-pdf-filename-helm-bibtex
	org-ref-default-bibliography (list zot-bib)
	org-ref-bibliography-notes "~/notes/bibnotes.org"
	org-ref-note-title-format "* TODO %y - %t\n :PROPERTIES:\n  :Custom_ID: %k\n  :NOTER_DOCUMENT: %F\n :ROAM_KEY: cite:%k\n  :AUTHOR: %9a\n  :JOURNAL: %j\n  :YEAR: %y\n  :VOLUME: %v\n  :PAGES: %p\n  :DOI: %D\n  :URL: %U\n :END:\n\n"
	org-ref-notes-directory org-notes
	org-ref-notes-function 'orb-edit-notes)

  (setq
   bibtex-completion-notes-path org-notes
   bibtex-completion-bibliography zot-bib
   bibtex-completion-pdf-field "file"
   bibtex-completion-notes-template-multiple-files
   (concat
    "#+TITLE: ${title}\n"
    "#+ROAM_KEY: cite:${=key=}\n"
    "* TODO Notes\n"
    ":PROPERTIES:\n"
    ":Custom_ID: ${=key=}\n"
    ":NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n"
    ":AUTHOR: ${author-abbrev}\n"
    ":JOURNAL: ${journaltitle}\n"
    ":DATE: ${date}\n"
    ":YEAR: ${year}\n"
    ":DOI: ${doi}\n"
    ":URL: ${url}\n"
    ":END:\n\n"
    )))

(use-package mixed-pitch
  :ensure t
  :hook (org-mode . mixed-pitch-mode)
  :config
(let* ((variable-tuple
        (cond ((x-list-fonts "ETBembo")         '(:font "ETBembo"))
              ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
              ((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
              ((x-list-fonts "Verdana")         '(:font "Verdana"))
              ((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
              (nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro."))))
       (base-font-color     (face-foreground 'default nil 'default))
       (headline           `(:inherit default :weight bold :foreground ,base-font-color)))

  (custom-theme-set-faces
   'user
   `(org-level-8 ((t ( ,@variable-tuple))))
   `(org-level-7 ((t ( ,@variable-tuple))))
   `(org-level-6 ((t ( ,@variable-tuple))))
   `(org-level-5 ((t ( ,@variable-tuple))))
   `(org-level-4 ((t ( ,@variable-tuple :height 1.1))))
   `(org-level-3 ((t ( ,@variable-tuple :height 1.15))))
   `(org-level-2 ((t ( ,@variable-tuple :height 1.2))))
   `(org-level-1 ((t ( ,@variable-tuple :height 1.25))))
   `(org-document-title ((t ( ,@variable-tuple :height 1.5 :underline nil))))))
(custom-theme-set-faces
 'user
'(variable-pitch ((t (:family "ETBembo" :height 180 :weight thin))))))

  ;; (set-frame-font "Consolas-14" nil t))


(provide 'memex)
;;; memex.el ends here
