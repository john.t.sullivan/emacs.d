(require 'package)

;;; Code:
(setq
 load-prefer-newer t
 package--init-file-ensured t
 package-archives '(("melpa" . "https://melpa.org/packages/")
		    ("gnu" . "https://elpa.gnu.org/packages/")
		    ("org" . "http://orgmode.org/elpa/")))
(package-initialize)

(when (display-graphic-p)
  (tool-bar-mode -1)
  (scroll-bar-mode -1))

(menu-bar-mode -1)
(global-linum-mode t)
(show-paren-mode t)
(global-hl-line-mode t)
(setq inhibit-splash-screen t)
(setq ring-bell-function 'ignore)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-and-compile 
  (require 'use-package))
(message "required use-package")

;; set some constants
(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/.emacs.d/")))

;; load in subfiles
(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))

(use-package which-key
  :demand t
  :delight
  :commands which-key-mode
  :init (which-key-mode)
  :config
  (progn
    (setq which-key-idle-delay 0.1
	   which-key-sort-order 'which-key-key-order-alpha)))

;; ========
;; Evil-mode settings
;; ========
(use-package general
  :demand t
  :commands general-evil-setup
  :config
  (progn
    (general-override-mode)
    (general-evil-setup)
    (general-create-definer spc-def :prefix "SPC")
    (spc-def :states '(normal visual)
      "k" 'helpful-key))
  (message "Loaded + configured general"))

(load-user-file "memex.el")
(load-user-file "clojure.el")
(load-user-file "completion.el")

(use-package evil
  :after general
  :ensure t
  :defer .1
  :init
  (setq evil-want-integration nil) ;; required by evil-collection
  (setq evil-want-keybinding nil)
  (setq evil-search-module 'evil-search)
  (setq evil-ex-complete-emacs-commands nil)
  (setq evil-vsplit-window-right t) ;; like vim's 'splitright'
  (setq evil-split-window-below t) ;; like vim's 'splitbelow'
  (setq evil-shift-round nil)
  (setq evil-want-C-u-scroll t)
  :config
  (evil-mode)

  ;; vim-like keybindings throughout emacs
  (use-package evil-collection
    :after evil
    :ensure t
    :config
    (evil-collection-init))

  ;; gc operator for commenting blocks
  (use-package evil-commentary
    :ensure t
    :bind (:map evil-normal-state-map
		("gc" . evil-commentary)))

  ;; visual hints while editing
  (use-package evil-goggles
    :ensure t
    :config
    (evil-goggles-use-diff-faces)
    (evil-goggles-mode))

  ;; like vim-surround
  (use-package evil-surround
    :ensure t
    :commands
    (evil-surround-edit
     evil-Surround-edit
     evil-surround-region
     evil-Surround-region)
    :init
    (evil-define-key 'operator global-map "s" 'evil-surround-edit)
    (evil-define-key 'operator global-map "S" 'evil-Surround-edit)
    (evil-define-key 'visual global-map "S" 'evil-surround-region)
    (evil-define-key 'visual global-map "gS" 'evil-Surround-region))

  (use-package evil-leader
    :ensure t
    :config
    (global-evil-leader-mode)
    (evil-leader/set-leader ","))


  ;; Esc escapes everywhere
  (evil-define-key nil evil-normal-state-map [escape] 'keyboard-quit)
  (evil-define-key nil evil-visual-state-map [escape] 'keyboard-quit)
  (evil-define-key nil evil-motion-state-map [escape] 'keyboard-quit)
  (evil-define-key nil minibuffer-local-map [escape] 'minibuffer-keyboard-quit)
  (evil-define-key nil minibuffer-local-ns-map [escape] 'minibuffer-keyboard-quit)
  (evil-define-key nil minibuffer-local-completion-map [escape] 'minibuffer-keyboard-quit)
  (evil-define-key nil minibuffer-local-must-match-map [escape] 'minibuffer-keyboard-quit)
  (evil-define-key nil minibuffer-local-isearch-map [escape] 'minibuffer-keyboard-quit)
  (global-set-key [escape] 'evil-exit-emacs-state)

  ;; Dvorak Window Navigation

  (general-define-key
   :keymaps 'evil-window-map
   "h" 'evil-window-up
   "m" 'evil-window-down
   "b" 'evil-window-left
   "w" 'evil-window-right
   "<up>" 'evil-window-up
   "<down>" 'evil-window-down
   "<left>" 'evil-window-left
   "<right>" 'evil-window-right
   "H" 'evil-window-move-very-top
   "M" 'evil-window-move-very-bottom
   "B" 'evil-window-move-far-left
   "W" 'evil-window-move-far-right))

;; ========
;; Look and feel
;; ========
(use-package zenburn-theme
  :ensure t
  :config
  (load-theme 'zenburn t)

  (add-to-list 'default-frame-alist '(font . "Consolas-14"))


  ;; Cursors
  (setq evil-emacs-state-cursor '("#CC9393" box))
  (setq evil-normal-state-cursor '("#7F9F7F" box))
  (setq evil-visual-state-cursor '("#DFAF8F" box))
  (setq evil-insert-state-cursor '("#CC9393" bar))
  (setq evil-replace-state-cursor '("#CC9393" bar))
  (setq evil-operator-state-cursor '("#CC9393" hollow)))

;; TODO need to configure latex so this is loaded when I try to set it
(setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))

;; (setq 'variable-pitch '(font . "LibreBaskerville-14"))
;; (setq 'fixed-pitch '(font . "Consolas-14"))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package magit
  :ensure t
  :config
  (spc-def :states 'normal
    "g" 'magit-status))


(use-package ivy
  :ensure t
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t
	enable-recursive-minibuffers t
	ivy-height 10
	ivy-count-format "%d/%d "
	ivy-re-builders-alist
	;; allow input not in order
        '((t   . ivy--regex-ignore-order)))
  ;; (global-set-key (kbd "C-<return>") 'ivy-immediate-done)
  ;; (global-set-key (kbd "C-c C-r") 'ivy-resume)
  ;; (global-set-key (kbd "<f6>") 'ivy-resume)
  (general-define-key
   :keymaps 'ivy-minibuffer-map
   "C-RET" 'ivy-immediate-done)

(use-package helpful)
  
  (use-package counsel
    :after (general helpful)
    :ensure t
    :config
  (setq
	ivy-initial-inputs-alist nil)
  ;; easy execution
  (spc-def
    :states '(normal visual emacs)
    "SPC" 'counsel-M-x
    "f" 'helpful-function
    "v" 'helpful-variable)))

(use-package ivy-bibtex
  :after ivy
  :ensure t
  :config
  ;; (comma-def :states 'normal
  ;;   "c" 'ivy-bibtex)
  )

(add-hook 'text-mode-hook #'visual-line-mode)


(use-package nix-mode
  :ensure t
  :mode "\\.nix\\'")

(use-package direnv
  :init
  (add-hook 'prog-mode-hook #'direnv-update-environment)
  :config
  (direnv-mode))

	  
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (makefile-executor company-lsp direnv lsp-ivy lsp-ui lsp-mode org-bullets company-posframe which-key helpful company-quickhelp company-box cider mixed-pitch haskell-mode nixos-options nix-company nix-mode magit zenburn-theme use-package rainbow-delimiters org-roam-bibtex org-ref org-journal ivy-bibtex general flycheck evil-surround evil-leader evil-goggles evil-commentary evil-collection deft counsel company-org-roam))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(evil-goggles-change-face ((t (:inherit diff-removed))))
 '(evil-goggles-delete-face ((t (:inherit diff-removed))))
 '(evil-goggles-paste-face ((t (:inherit diff-added))))
 '(evil-goggles-undo-redo-add-face ((t (:inherit diff-added))))
 '(evil-goggles-undo-redo-change-face ((t (:inherit diff-changed))))
 '(evil-goggles-undo-redo-remove-face ((t (:inherit diff-removed))))
 '(evil-goggles-yank-face ((t (:inherit diff-changed))))
 '(org-document-title ((t (:font "ETBembo" :height 1.5 :underline nil))))
 '(org-level-1 ((t (:font "ETBembo" :height 1.25))))
 '(org-level-2 ((t (:font "ETBembo" :height 1.2))))
 '(org-level-3 ((t (:font "ETBembo" :height 1.15))))
 '(org-level-4 ((t (:font "ETBembo" :height 1.1))))
 '(org-level-5 ((t (:font "ETBembo"))))
 '(org-level-6 ((t (:font "ETBembo"))))
 '(org-level-7 ((t (:font "ETBembo"))))
 '(org-level-8 ((t (:font "ETBembo"))))
 '(variable-pitch ((t (:family "ETBembo" :height 180 :weight thin)))))
