(use-package company
  :ensure t
  :config
  (setq company-idle-delay 0.0
	company-minimum-prefix-length 2)

  (use-package company-posframe 
    :after company
    :hook (company-mode . company-posframe-mode))
  (general-unbind
    :keymaps 'company-active-map
    "TAB" "<tab>" "RET" "<return>")
  (general-define-key
   :keymaps 'company-active-map
   "TAB" 'company-complete-common-or-cycle
   "RET" 'company-complete-selection)
  (global-company-mode 1))

(use-package lsp-mode
  :ensure t
  :after (direnv evil)
  :hook (
	 (clojure-mode . lsp)
	 (emacs-lisp-mode . lsp)
	 (lsp-mode . lsp-enable-which-key-integration))
  :config
  (setq lsp-prefer-flymake nil
	lsp-enable-snippet nil)
  (setq lsp-log-io t)
  ; LSP will watch all files in the project
  ; directory by default, so we eliminate some
  ; of the irrelevant ones here, most notable
  ; the .direnv folder which will contain *a lot*
  ; of Nix-y noise we don't want indexed.
  (setq lsp-file-watch-ignored '(
				 "[/\\\\]\\.direnv$"
					; SCM tools
				 "[/\\\\]\\.git$"
				 "[/\\\\]\\.hg$"
				 "[/\\\\]\\.bzr$"
				 "[/\\\\]_darcs$"
				 "[/\\\\]\\.svn$"
				 "[/\\\\]_FOSSIL_$"
					; IDE tools
				 "[/\\\\]\\.idea$"
				 "[/\\\\]\\.ensime_cache$"
				 "[/\\\\]\\.eunit$"
				 "[/\\\\]node_modules$"
				 "[/\\\\]\\.fslckout$"
				 "[/\\\\]\\.tox$"
				 "[/\\\\]\\.stack-work$"
				 "[/\\\\]\\.bloop$"
				 "[/\\\\]\\.metals$"
				 "[/\\\\]target$"
					; Autotools output
				 "[/\\\\]\\.deps$"
				 "[/\\\\]build-aux$"
				 "[/\\\\]autom4te.cache$"
				 "[/\\\\]\\.reference$")))

(use-package lsp-ui
  :ensure t
  :after lsp-mode
  :commands lsp-ui-mode)
(use-package lsp-ivy
  :ensure t
  :after lsp-mode
  :commands lsp-ivy-workspace-symbol)


(provide 'completion)
